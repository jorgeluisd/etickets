<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
	Route::resource('ticket','TicketsController');
	Route::get('ticket/{client}/create', 'TicketsController@create')
		->name('ticket.create');
	Route::get('ticket/{ticket}/destroy', 'TicketsController@destroy')
		->name('ticket.destroy');
	Route::get('ticket/{ticket}/reprint', 'TicketsController@rePrint')
		->name('ticket.reprint');

	Route::get('ticket/qr-code/generate/{ticket}', 'TicketsController@generateQR')
		->name('ticket.generate-qr');

	Route::resource('client','ClientsController');

	//Rutas solo admin
	Route::post('ticket/{ticket}/checkin', 'TicketsController@checkin')
		->name('ticket.checkin');
	Route::get('client/{client}/destroy', 'ClientsController@destroy')
		->name('client.destroy');
	Route::get('client/{client}/active', 'ClientsController@active')
		->name('client.active');
});
