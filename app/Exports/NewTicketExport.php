<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class NewTicketExport implements FromView
{
	use Exportable;

	private $tickets;
	private $img;

	public function __construct($tickets)
	{
		$this->tickets = $tickets;
	}

    public function view(): View
    {
    	// dd($this->tickets);
        return view('pdf.new-ticket', [
            'tickets' => $this->tickets,
        ]);
    }
}