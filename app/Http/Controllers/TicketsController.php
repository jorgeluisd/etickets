<?php

namespace App\Http\Controllers;

use App\Exports\NewTicketExport;
use App\Http\Requests\TicketRequest;
use App\Models\Client;
use App\Models\Event;
use App\Models\MethodPay;
use App\Models\Ticket;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Ticket::with(['client','event','user','methodPay'])
            ->oldest()
            ->search($request->get('search'))
            ->paginate(25);
        return view('tickets.index')
                ->with([
                    'data' => $data
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $events = Event::all();
        $methodPays = MethodPay::pluck('name','id');
        //mejorar tipo de ticket
        $ticketTypes = TicketType::select(DB::raw("concat(name,' (USD',price,')') AS name"), 'id', 'price')
            ->get();

        return view('tickets.create')
            ->with([
                'client'=>$client,
                'events'=>$events,
                'methodPays'=>$methodPays,
                'ticketTypes' => $ticketTypes
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {
        if ($request->all()) {

        DB::beginTransaction();

            $arr = [];

            for ($i=0; $i < $request->qty; $i++) { 
                $ticket = new Ticket();
                $ticket->fill($request->all());
                $ticket->serial = $this->getSerial();
                if (!$ticket->save()) {
                    DB::rollback();
                }
                $arr[] = $ticket->load(['event']);
            }
        }
        DB::commit();
        // return (new NewTicketExport($arr,$img))->download('ticket.pdf', \Maatwebsite\Excel\Excel::DOMPDF);

        return view('pdf.new-ticket', [
            'tickets' => $arr,
        ]);




        //Enviar mail
        //Generar PDF
        return redirect()->route('ticket.generate-qr',$ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id)->load(['client','event','user','methodPay','ticketType']);
        
        $events = Event::pluck('name','id');
        $methodPays = MethodPay::pluck('name','id');
        //mejorar tipo de ticket
        $ticketTypes = TicketType::select(DB::raw("concat(name,' (USD',price,')') AS name"), 'id')
            ->pluck('name','id');

        return view('tickets.show')
            ->with([
                'ticket'=>$ticket,
                'events'=>$events,
                'methodPays'=>$methodPays,
                'ticketTypes' => $ticketTypes
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();
        flash('Anulado con éxito')->success()->important();
        return redirect()->route('ticket.index');   
    }

    public function generateQR(Ticket $ticket)
    {
        $event = $ticket->event;
        $arr = [
            'id' => $ticket->id,
            'nombre' => $ticket->client->name,
            'cedula' => $ticket->client->document,
            'fecha_compra' => $ticket->created_at,
        ];
        return view('qrCode')
            ->with([
                'ticket' => json_encode($arr),
                'event' => $event,
            ]);
    }

    public function getSerial()
    {
        $tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        $serial = '';

        for ($i = 0; $i < 4; $i++) {
            for ($j = 0; $j < 5; $j++) {
                $serial .= $tokens[rand(0, 35)];
            }

            if ($i < 3) {
                $serial .= '-';
            }
        }

        return $serial;
    }

    public function checkin(Request $request,$id)
    {
        $clientId = $request->client_id;
        $serial = $request->serial;
        $eventId = $request->event_id;

        $ticket = Ticket::find($id);

        if ($ticket) {
            if ($ticket->client->id == $clientId && $ticket->serial == $serial && $ticket->event->id == $eventId  ) {
                $ticket->checkin_at=\Carbon\Carbon::now();
                $ticket->save();
                return redirect()->back();
            }
        }
        return 'error';
    }

    public function rePrint($id)
    {
        $ticket = Ticket::find($id)->load(['client','event','user','methodPay','ticketType']);
        $arr[] = $ticket;

        // return (new NewTicketExport($arr))->download('ticket.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
        return view('pdf.new-ticket', [
            'tickets' => $arr,
        ]);
    }
}
