<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Client::oldest()
            ->search($request->get('search'))
            ->paginate(25);
        return view('clients.index')
                ->with([
                    'data' => $data
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $client = new Client();
        $client->fill($request->all());
        $client->active = ($request->has('active') && $request->active=='on')? true : false;
        $client->save();

        // Cache::forget('paper-size');
        
        flash('Creado con éxito')->success()->important();
        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clients.edit')
        ->with([
            'client' => $client
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, Client $client)
    {
        $client->fill($request->all());
        $client->active = $request->has('active');
        if (!$client->save()) {
            throw new HttpException(500);
        }

        flash('Actualizado con éxito')->success()->important();
        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        flash('Eliminado con éxito')->success()->important();
        return redirect()->route('client.index');
    }

    public function active(Client $client)
    {
        $client->active = !$client->active;
        if (!$client->save()) {
            throw new HttpException(500);
        }

        flash('Activado / Desactivado con éxito')->success()->important();
        return redirect()->route('client.index');
    }
}
