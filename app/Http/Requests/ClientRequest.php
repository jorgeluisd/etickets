<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('client')->id ?? null;
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'document' => 'required|string|max:10|unique:clients,document,'.$id,
            'address' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'celular' => 'nullable|string|max:15', 
        ];
    }
}
