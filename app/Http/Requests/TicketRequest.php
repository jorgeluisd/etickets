<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|integer',
            'event_id' => 'required|integer',
            'method_pay_id' => 'required|integer',
            'ticket_type_id' => 'required|integer',
            'qty' => 'required|integer|max:10',
            'ref' => 'nullable|string|max:255', 
        ];
    }
}
