<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Client extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['document', 'name', 'address', 'phone', 'celular', 'email',];

    protected static function boot () {
		parent::boot();
		static::creating(function (Client $client) {
			if( ! \App::runningInConsole()) {
				$client->user_id = Auth::id();
			}
		});
	}

	public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /*********SCOPES*******BUSQUEDAS*/

    public function scopeSearch($query, $value){
        if (trim($value) != '') {   
            return $query
            	->where('document','like',"%$value%")
            	->orWhere('email','like',"%$value%")
                ->orWhere('name','like',"%$value%");
        }
    }
    /*********SCOPES*******FIN*/

}
