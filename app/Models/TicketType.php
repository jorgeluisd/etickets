<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
	protected $fillable = ['name','price'];

	protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return "{$this->name} (USD{$this->price})";
    }
}
