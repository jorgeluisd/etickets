<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
	const RESELLER = 2;
	const OPERATOR = 3;
}
