<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
	use SoftDeletes;
	
	protected $fillable = ['client_id', 'event_id', 'user_id','method_pay_id','ticket_type_id', 'ref'];
    
    protected static function boot () {
		parent::boot();
		static::creating(function (Ticket $client) {
			if( ! \App::runningInConsole()) {
				$client->user_id = Auth::id();
			}
		});
	}

	public function client()
	{
		return $this->belongsTo(Client::class)->withDefault(new Client());
	}

	public function event()
	{
		return $this->belongsTo(Event::class)->withDefault(new Event());
	}

	public function user()
	{
		return $this->belongsTo(User::class)->withDefault(new User());
	}

	public function methodPay()
	{
		return $this->belongsTo(MethodPay::class)->withDefault(new MethodPay());
	}

	public function ticketType()
	{
		return $this->belongsTo(TicketType::class)->withDefault(new ticketType());
	}

	/*********SCOPES*******BUSQUEDAS*/

    public function scopeSearch($query, $value){
        if (trim($value) != '') {   
            return $query
            	->where('id','like',"%$value%")
            	->orWhereHas('methodPay', function($query) use ($value) {
            		$query->where('name','like',"%$value%");
            	})
            	->orWhereHas('client', function($query) use ($value) {
            		$query->where('document','like',"%$value%")
            			->orWhere('name','like',"%$value%");
            	});
        }
    }
    /*********SCOPES*******FIN*/
}
