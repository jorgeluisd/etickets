<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('method_pays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('ticket_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('price', 9, 2);
            $table->unsignedBigInteger('event_id');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('event_id');
            $table->unsignedInteger('method_pay_id');
            $table->unsignedInteger('ticket_type_id');
            $table->unsignedBigInteger('user_id');
            $table->string('ref')->nullable();
            $table->dateTime('checkin_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('method_pays');
    }
}
