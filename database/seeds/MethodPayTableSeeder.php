<?php

use App\Models\MethodPay;
use Illuminate\Database\Seeder;

class MethodPayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MethodPay::create([
        	'name' => 'Efectivo',
        ]);
        MethodPay::create([
        	'name' => 'Cheque',
        ]);
        MethodPay::create([
        	'name' => 'Transferencia',
        ]);
        MethodPay::create([
        	'name' => 'Punto de venta',
        ]);
        MethodPay::create([
        	'name' => 'Otro',
        ]);
    }
}
