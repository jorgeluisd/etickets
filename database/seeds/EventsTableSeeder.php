<?php

use App\Models\Event;
use App\Models\TicketType;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::create([
        	'name' => 'Nacho De Vuelta a Casa',
        	'address' => 'Hotel Maremares, Lecheria',
        	'date' => '2019-12-01',
        	'time' => '19:00',
        ]);

        TicketType::create([
            'name' => 'Gold',
            'price' => 150,
            'event_id' => 1
        ]);

        TicketType::create([
            'name' => 'Vip',
            'price' => 120,
            'event_id' => 1
        ]);

        TicketType::create([
            'name' => 'General',
            'price' => 60,
            'event_id' => 1
        ]);
    }
}
