<?php

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
        	'name' => 'Jorge Diaz',
        	'email' => 'diazjorgeluis10@gmail.com',
        	'celular' => '123123',
        	'phone' => '123123',
        	'document' => '17733181',
        	'address' => 'Chuparin',
        	'user_id' => 1,
        ]);
        Client::create([
        	'name' => 'Lerida Diaz',
        	'email' => 'diazjorgeluis10@gmail.com',
        	'celular' => '123123',
        	'phone' => '123123',
        	'document' => '3671180',
        	'address' => 'Chuparin',
        	'user_id' => 1,
        ]);
    }
}
