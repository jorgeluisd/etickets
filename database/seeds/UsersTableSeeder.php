<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'resseller']);
        Role::create(['name' => 'operator']);

        User::create([
        	'name' => 'Jorge Diaz',
        	'email' => 'diazjorgeluis10@gmail.com',
        	'password' => '123123',
            'role_id' => 1,
        ]);

        User::create([
            'name' => 'Operador',
            'email' => 'operador@gmail.com',
            'password' => '123123',
            'role_id' => 3,
        ]);
    }
}
