<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
    
<div class="visible-print text-center">
	<h1>E-ticket</h1>
	<h2>{{ $event->name }}</h2>

	<p>Fecha: {{ $event->date }} Hora: {{ $event->time }}</p>
     
    {!! QrCode::size(250)->generate($ticket); !!}
     
    <p>Todos los derechos reservados</p>
</div>
    
</body>
</html>