@extends('layouts.app')

@section('content')
    <div class="card-header text-white bg-primary">
    Tickets
    </div>
    <div class="card-body">
        <h5 class="card-title">Vender Ticket</h5>

        {!! Form::open(['route'=>'ticket.store', 'method'=>'POST', 'class'=> 'mb-5']) !!}
			<div class="row">
				<div class="col-lg-4 col-xl-5">
					{!! Form::hidden('client_id',$client->id,[]) !!}
	                <div class="form-group">
	                    {!! Form::label('name','Nombre completo') !!}
	                    {!! Form::text('name',$client->name,['class'=>'form-control','disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('email','Correo electronico') !!}
	                    {!! Form::email('email',$client->email,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-2">
	                <div class="form-group">
	                    {!! Form::label('document','Documento de identificacion') !!}
                    	{!! Form::text('document',$client->document,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('address','Direccion') !!}
                    	{!! Form::text('address',$client->address,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('phone','Telefono') !!}
                    	{!! Form::text('phone',$client->phone,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-2">
	                <div class="form-group">
	                    {!! Form::label('celular','Celular') !!}
                    	{!! Form::text('celular',$client->celular,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>

			<div>
				<ticket-prices
					:ticket-types="{{ $ticketTypes }}"
					:events="{{ $events }}"
				/>
			</div>

			<div class="row">
				<div class="col-xl-3">
	                <div class="form-group">
	                    {!! Form::label('method_pay_id','Metodo de pago') !!}
                    	{!! Form::select('method_pay_id',$methodPays,'',['class'=>'form-control form-control-alt', 'placeholder'=>'Seleccione...']) !!}
	                </div>
				</div>
				<div class="col-xl-9">
	                <div class="form-group">
	                    {!! Form::label('ref','Referencia') !!}
                    	{!! Form::text('ref',null,['class'=>'form-control',]) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    <a href="{{ route('client.index') }}" class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Regresar' data-original-title='Top Tooltip' role='button'> Regresar </a>
                    	<button type="submit" class="btn btn-primary">Guardar</button>
	                </div>
				</div>
			</div>
		{{ Form::close() }}
            
    </div>
    
@endsection