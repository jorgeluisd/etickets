@extends('layouts.app')

@section('content')
    <div class="card-header text-white bg-primary">
    Tickets
    </div>
    <div class="card-body">
        <h5 class="card-title">Listado de tickets</h5>

        <a class="btn btn-primary" href="{{ Route('client.index') }}" role="button">Nuevo</a>

        <h4 class="m-4"><strong>Filtros</strong></h4>
        {!! Form::open(['route'=>'ticket.index', 'method'=>'GET', 'class' => 'form-inline m-2']) !!}

        <div class="form-group mx-sm-3 mb-2">
            {!! Form::label('search','Busqueda') !!}
            {!! Form::text('search',null,['class'=>'form-control ml-2', 'placeholder'=>'Busqueda']) !!}
        </div>
        <button type="submit" class="btn btn-primary btn-flat mb-2" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fa fa-search"></i></button>

        {!! Form::close() !!}

        <table class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Evento</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Metodo pago</th>
                    <th scope="col">Referencia</th>
                    <th scope="col">Vendido por</th>
                    <th scope="col">Fecha venta</th>
                    <th scope="col">Checkin</th>
                    <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $value)
                    <tr>
                        <th scope="row">{{ $value->id }}</th>
                        <td>{{ $value->event->name }}</td>
                        <td>{{ $value->client->document }}</td>
                        <td>{{ $value->client->name }}</td>
                        <td>{{ $value->methodPay->name }}</td>
                        <td>{{ $value->ref }}</td>
                        <td>{{ $value->user->name }}</td>
                        <td>{{ $value->created_at }}</td>
                        <td>
                            @if(!$value->checkin_at)
                                <span class="badge badge-success">Sin chequear</span>
                            @else
                                <span class="badge badge-danger">Chequeado</span>
                            @endif
                        </td>
                        <td>
                            <span class="badge badge-default">
                                <a href="{{ Route('ticket.reprint', $value) }}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Reimprimir ticket" data-original-title="Top Tooltip" target="_blank">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Cambiar Status" data-original-title="Top Tooltip" onclick="return confirm('Seguro que desea cambiar checkin?...')">
                                    <i class="fa fa-code-branch" aria-hidden="true"></i>
                                </a>
                                @if(Auth::user()->isAdmin())
                                    <a href="{{ Route('ticket.destroy', $value) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Anular" data-original-title="Top Tooltip" onclick="return confirm('Seguro que desea anular?...')">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->appends(Request::query())->links() }}
    </div>


    
@endsection