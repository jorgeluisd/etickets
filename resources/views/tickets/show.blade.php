@extends('layouts.app')

@section('content')
    <div class="card-header text-white bg-primary">
    Tickets
    </div>
    <div class="card-body">
        <h5 class="card-title">Mostrar Ticket</h5>

        @if(!$ticket->checkin_at)
        	<div class="alert alert-success" role="alert">CheckIn disponible</div>
       	@else
       		<div class="alert alert-danger" role="alert">Checkin Realizado</div>
        @endif

        {!! Form::open(['route'=>['ticket.checkin',$ticket], 'method'=>'POST', 'class'=> 'mb-5']) !!}
			<div class="row">
				<div class="col-lg-4 col-xl-5">
					{!! Form::hidden('client_id',$ticket->client->id,[]) !!}
					{!! Form::hidden('serial',Request()->get('serial'),[]) !!}
					{!! Form::hidden('event_id',Request()->get('event'),[]) !!}
	                <div class="form-group">
	                    {!! Form::label('name','Nombre completo') !!}
	                    {!! Form::text('name',$ticket->client->name,['class'=>'form-control','disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('email','Correo electronico') !!}
	                    {!! Form::email('email',$ticket->client->email,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-2">
	                <div class="form-group">
	                    {!! Form::label('document','Documento de identificacion') !!}
                    	{!! Form::text('document',$ticket->client->document,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('address','Direccion') !!}
                    	{!! Form::text('address',$ticket->client->address,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('phone','Telefono') !!}
                    	{!! Form::text('phone',$ticket->client->phone,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-lg-4 col-xl-2">
	                <div class="form-group">
	                    {!! Form::label('celular','Celular') !!}
                    	{!! Form::text('celular',$ticket->client->celular,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>


			<div class="row">
				<div class="col-xl-6">
	                <div class="form-group">
	                	{!! Form::label('event_id','Evento') !!}
                    	{!! Form::text('event_id',$ticket->event->name,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-xl-6">
	                <div class="form-group">
	                	{!! Form::label('ticket_type_id','Evento') !!}
                    	{!! Form::text('ticket_type_id',$ticket->ticketType->full_name,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-xl-3">
	                <div class="form-group">
	                	{!! Form::label('method_pay_id','Metodo de pago') !!}
                    	{!! Form::text('method_pay_id',$ticket->methodPay->name,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
				<div class="col-xl-9">
	                <div class="form-group">
	                    {!! Form::label('ref','Referencia') !!}
                    	{!! Form::text('ref',$ticket->ref,['class'=>'form-control', 'disabled']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    <a href="{{ route('client.index') }}" class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Regresar' data-original-title='Top Tooltip' role='button'> Regresar </a>
	                    @if( (Auth::user()->isOperator() || Auth::user()->isAdmin()) && !$ticket->checkin_at)
                    		<button type="submit" class="btn btn-success" onclick="return confirm('Seguro que desea hacer checkin?...')">Checkin</button>
                    	@endif
	                </div>
				</div>
			</div>
		{{ Form::close() }}
            
    </div>
    
@endsection