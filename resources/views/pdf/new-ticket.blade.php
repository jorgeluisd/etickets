<!DOCTYPE html>
<html>
<head>
	<title>Eticket</title>
	<style type="text/css">
		.page-break {
			page-break-before: always;
		}

		.left{
		    float: left;
		}
		.right{
		    float: right;
		}
		.center{
			text-align: center;
		}
	</style>
</head>
<body>
    

	@foreach($tickets as $ticket)
		<div class="visible-print text-center">

			<div class="container">
				<div class="left">
					<img src="{{ asset('images/nacho.png') }}" style="width: 150px">
				</div>
				<div class="right">
					{!! QrCode::size(150)->generate(Route('ticket.show',$ticket->id)."?serial=$ticket->serial&event=".$ticket->event->id) !!}
				</div>

				<div class="center">
					<h1>E-ticket</h1>
					<h2>{{ $ticket->event->name }}</h2>

					<p><strong>Fecha: {{ $ticket->event->date }} Hora: {{ $ticket->event->time }}</strong></p>
				</div>

			</div>


			<br>
			
			<div class="page-break"></div>
			<hr>
		     
		</div>
	@endforeach
    
</body>
</html>