@extends('layouts.app')

@section('content')
    <div class="card-header text-white bg-primary">
    Clientes
    </div>
    <div class="card-body">
        <h5 class="card-title">Listado de clientes</h5>

        <a class="btn btn-primary" href="{{ Route('client.create') }}" role="button">Nuevo</a>

        <h4 class="m-4"><strong>Filtros</strong></h4>
        {!! Form::open(['route'=>'client.index', 'method'=>'GET', 'class' => 'form-inline m-2']) !!}

        <div class="form-group mx-3 mb-2">
            {!! Form::label('search','Busqueda') !!}
            {!! Form::text('search',null,['class'=>'form-control ml-2', 'placeholder'=>'Busqueda']) !!}
        </div>
        <button type="submit" class="btn btn-primary btn-flat mb-2" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fa fa-search"></i></button>

        {!! Form::close() !!}

        <table class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Tlfn</th>
                    <th scope="col">Celular</th>
                    <th scope="col">Accion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $value)
                    <tr>
                        <th scope="row">{{ $value->id }}</th>
                        <td>{{ $value->document }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->phone }}</td>
                        <td>{{ $value->celular }}</td>
                        <td>
                            <span class="badge badge-default">
                                <a href="{{ Route('ticket.create', $value->id) }}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Vender ticket" data-original-title="Top Tooltip">
                                    <i class="fa fa-chart-bar" aria-hidden="true"></i>
                                </a>

                                <a href="{{route('client.edit',$value)}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Top Tooltip">
                                    <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                </a>

                                <a href="{{ Route('client.active',$value) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Cambiar Status" data-original-title="Top Tooltip" onclick="return confirm('Seguro que desea deshabilitar?...')">
                                    <i class="fa fa-code-branch" aria-hidden="true"></i>
                                </a>

                                <a href="{{ Route('client.destroy',$value) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Top Tooltip" onclick="return confirm('Seguro que desea eliminar?...')">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                            </span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->appends(Request::query())->links() }}
    </div>


    
@endsection