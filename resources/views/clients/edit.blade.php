@extends('layouts.app')

@section('content')
    <div class="card-header text-white bg-primary">
    Clientes
    </div>
    <div class="card-body">
        <h5 class="card-title">Editar cliente</h5>

        {!! Form::open(['route'=>['client.update',$client], 'method'=>'PUT', 'class'=> 'mb-5']) !!}
			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('name','Nombre completo') !!}
	                    {!! Form::text('name',$client->name,['class'=>'form-control', 'placeholder'=>'Ingrese nombre completo']) !!}
	                </div>
				</div>
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('email','Correo electronico') !!}
	                    {!! Form::email('email',$client->email,['class'=>'form-control', 'placeholder'=>'Ingrese correo electronico']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('document','Documento de identificacion') !!}
                    	{!! Form::text('document',$client->document,['class'=>'form-control', 'placeholder'=>'Ingrese cedula de identidad']) !!}
	                </div>
				</div>
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('address','Direccion') !!}
                    	{!! Form::text('address',$client->address,['class'=>'form-control', 'placeholder'=>'Ingrese direccion']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('phone','Telefono') !!}
                    	{!! Form::text('phone',$client->phone,['class'=>'form-control', 'placeholder'=>'Ingrese telefono']) !!}
	                </div>
				</div>
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    {!! Form::label('celular','Celular') !!}
                    	{!! Form::text('celular',$client->celular,['class'=>'form-control', 'placeholder'=>'Ingrese celular']) !!}
	                </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-xl-5">
	                <div class="form-group">
	                    <div class="custom-control custom-switch custom-control-inline custom-control-success">
	                        <input type="checkbox" class="custom-control-input" id="activo" name="active" checked>
	                        <label class="custom-control-label" for="activo">Activo</label>
                    	</div>
	                </div>
	                <div class="form-group">
	                    <a href="{{ route('client.index') }}" class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Regresar' data-original-title='Top Tooltip' role='button'> Regresar </a>
                    	<button type="submit" class="btn btn-primary">Guardar</button>
	                </div>
				</div>
			</div>
		{{ Form::close() }}
            
    </div>


    
@endsection